# Arista BGP EVPN VXLAN Ansible Automation

The Project is to show the automation of an Arista EVPN BGP using Ansible
with the integration of Gitlab for source control, Gitlab Runners for CI and Ansible Tower 3.8.

![Topology](/docs/images/arista-ansible-automation.drawio.png)

This is an ongoing work in progress. Feedback would be most appreciated.